This repository serves as a container for all cppTango ci docker repositories.

Each repository in https://gitlab.com/tango-controls/docker/ci/cpptango which
produces a docker image is contained here as submodule.

The purpose is to leverage the git submodule capabilities for easy scripting.

### Common tasks

1. Update to the latest commit

```
git submodule update --remote --recursive
git submodule foreach 'git checkout origin/main'
git commit -m "Update all submodules" .
```

2. Add a new tag (XXX is a integer > 0)

```
git submodule foreach 'git tag vXXX; git push origin vXXX'
```

or add a new tag with incremented version:

```
git submodule foreach 'git tag v$(expr $(git tag --sort="version:refname" | tail -n1 | grep -oP "[[:digit:]]+") \+ 1)'
```

Push the latest tag:

```
git submodule foreach 'git push origin $(git tag --sort="version:refname" | tail -n1)'
```

3. Update scripts folder

```
./update-scripts.sh $script_repo_rev
```

3. Updating a single repository

```
cd YYY (e.g. debian12)
git checkout -b update
# hack hack hack
# commit changes
git push
# Let CI build the image
# Test it
# Merge MR
git fetch
git checkout origin/main
git tag vXXX
# from the top level directory
git commit -m ""Update YYY" YYY
```

4. Generate diff from all repositories

```
git submodule foreach 'git fetch; git checkout origin/XXX || :'
git submodule foreach 'echo -n "\n"; git --no-pager log -p origin/main..' > super.diff
```

5. List latest tags

```
git submodule foreach 'git tag --sort="version:refname" | tail -n1'
```
