#!/bin/sh

# Update the scripts folder in all submodules to the given revision, create a branch, commit the changes and
# force push it

set -ex

export scripts_ref=$1
export branch=update

# fetch updates from remotes in all submodules
git submodule update --remote --recursive
git submodule foreach --recursive "git checkout origin/main"

git submodule foreach "git checkout -B ${branch}"

git submodule foreach "(cd scripts && git checkout ${scripts_ref}) || :"
git submodule foreach "git commit -m \"Update scripts\" scripts || :"
git submodule foreach "git push --force origin HEAD:${branch}"
